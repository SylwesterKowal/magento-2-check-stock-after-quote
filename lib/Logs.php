<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 18.08.2018
 * Time: 16:46
 */

namespace M21\CheckStockAfterQuote\lib;

class Logs
{
    public $loggerl;

    public function __construct($file = 'test.log')
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/' . $file);
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
    }
}