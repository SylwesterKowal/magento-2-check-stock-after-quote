<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 18.08.2018
 * Time: 17:18
 */

namespace M21\CheckStockAfterQuote\lib;

use \Magento\Framework\HTTP\Client\Curl;
use M21\CheckStockAfterQuote\lib\Logs;

class Client extends Curl
{


    /**
     * @param string $ip
     * @return string
     */
    public function getStocks($url)
    {
        $this->_curlUserOptions = [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => ['Host: 148.251.156.43', "Content-Type: application/json"]
        ];

        $this->get($url);
        return $this->getBody();
    }

    public
    function restRequestCurl($url, $productData, $requestType)
    {
        $log = new Logs('client_curl.txt');
        $ch = curl_init($url);

        $curlOptions = [
            CURLOPT_CUSTOMREQUEST => $requestType,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => ['Host: 148.251.156.43', "Content-Type: application/json"]
//            CURLOPT_HTTPHEADER => array("Content-Type: application/json", "Authorization: Bearer " . $this->accessToken)
        ];
        if ($productData) {
            $curlOptions[CURLOPT_POSTFIELDS] = json_encode($productData);
            $log->logger->info(print_r($curlOptions[CURLOPT_POSTFIELDS], true));
        }

        curl_setopt_array($ch, $curlOptions);

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
}
