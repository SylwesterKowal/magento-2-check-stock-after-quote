<?php


namespace M21\CheckStockAfterQuote\Observer\Checkout;

use M21\CheckStockAfterQuote\lib\Logs;
use M21\CheckStockAfterQuote\lib\Client;
use Magento\Framework\Event\ObserverInterface;


class SubmitBefore implements \Magento\Framework\Event\ObserverInterface
{

    public $stockRegistry;

    function __construct(
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
    )
    {
        $this->stockRegistry = $stockRegistry;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    )
    {
        try {

            $test = true;

            $log = new Logs('checkout_submit_before.txt');
            $client = new Client();
            $url = 'http://www.shopla.pl/userprocess.php';

            $items = $observer->getQuote()->getAllItems();
            $param = ['mu_integrator_erp' => 'getShoplaStock'];
            $order = [];
            if ($test) $url .= '?mu_integrator_erp=getShoplaStock';
            foreach ($items as $item) {
//            echo 'ID: '.$item->getProductId().'<br />';

                $param['code'][] = $item->getSku();
                $order[$item->getSku()] = $item->getQty();
                if ($test) $url .= '&code[]=' . $item->getSku();

            }

//            if (!$test) {
//                $shopla = $client->restRequestCurl($url, $param, 'GET');
//            } else {
//                $log->logger->info($url);
//                $shopla = $client->getStocks($url);
//            }
//            $log->logger->info(print_r($shopla, true));

            $shopla = json_decode('{"status":"OK","result":{"W.S.W.0000543.ba":"1"}}', true);

            if (isset($shopla['result']) && is_array($shopla['result'])) {
                foreach ($shopla['result'] as $sku => $shopla_stock) {
                    $magento_stock = $this->stockRegistry->getStockItemBySku($sku)->getQty();
                    $log->logger->info("zamowiono " . $sku . ': ' .$order[$sku]. ' - stany: ' . $magento_stock.' / '.$shopla_stock);
                    /**
                     * Uaktualnij stany tylko wtedy gdy zakupiona ilość jest
                     * 1. Stan Magento jest różny od stanu Shopli
                     * 2. Zakupiona ilość <= Magento
                     * 3. Zakupiona ilość > Shopli
                     */
                    if (isset($order[$sku]) && $magento_stock >= $shopla_stock && $order[$sku] <= $magento_stock && $order[$sku] > $shopla_stock){
                        $this->updateStockBySku($sku, $shopla_stock);
                    }else if(isset($order[$sku]) && $magento_stock > $shopla_stock && $order[$sku] <= $magento_stock && $order[$sku] >= $shopla_stock) {
                        $this->updateStockBySku($sku, $shopla_stock);
                    }else{
                        $log->logger->info("Stan jest odpowiedni");
                    }
                }
            } else {
                $log->logger->info("Błędne dane z Shopla!!!");
            }

        } catch (Exception $e) {
//            throw new \Magento\Framework\Exception\LocalizedException('Wystąpił wyjątek nr ' . $e->getCode() . ', komunikat:' . $e->getMessage());
        }
    }

    private function updateStockBySku($sku, $qty)
    {
        $stockItem = $this->stockRegistry->getStockItemBySku($sku);
        $stockItem->setQty($qty);
        $stockItem->setIsInStock((bool)$qty); // this line
        $this->stockRegistry->updateStockItemBySku($sku, $stockItem);
    }
}
